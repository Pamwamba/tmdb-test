import { useState } from 'react';
import {
  QueryClient,
  QueryClientProvider,
} from 'react-query'
import { ReactQueryDevtools } from 'react-query/devtools'
import {
  BrowserRouter as Router,
  Route,
  Routes,
} from "react-router-dom";
import useLocalStorage from 'use-local-storage'

import { Movie } from './types/Movie';
import MovieDetails from './pages/MovieDetails';
import MovieList from './pages/MovieList';

import './App.css';
import Header from './components/header';

const queryClient = new QueryClient();

function App() {
  const [movies, setMovies] = useState<Movie[]>([]);
  const defaultDark = window.matchMedia('(prefers-color-scheme: dark)').matches;
  const [dark, setDark] = useLocalStorage('theme', defaultDark ? true : false);

  const switchTheme = () => {
    const newTheme = !dark ? true : false;
    setDark(newTheme);
  }

  return (
    <QueryClientProvider client={queryClient}>
      <Router>
        <div className="App" data-theme={dark ? 'dark' : 'light'}>
          <Header switchTheme={() => switchTheme()} getResults={(m: Movie[]) => setMovies(m)}/>
          <div className='content'>
            <Routes>
              <Route path='/' element={<MovieList moviesList={movies} />} />
              <Route path='/:id' element={<MovieDetails />} />
            </Routes>
          </div>
        </div>
      </Router>
      <ReactQueryDevtools initialIsOpen={false} />
    </QueryClientProvider>
  );
}

export default App;

import { MovieInfo } from "../types/MovieInfo";

const BASE_URL: string = (process.env.REACT_APP_BASE_URL as string);
const API_KEY: string = (process.env.REACT_APP_API_KEY as string);

/**
 * Get movies from typed text.
 * @param {string} query
 * @returns {Movie[]}
 */
// API search function
export async function searchMovie(search: string) {
  return fetch(
    `${BASE_URL}?apikey=${API_KEY}&s=${search}`,
    {
      method: "GET",
    }
  )
    .then((r) => r.json())
    .then((r) => r.Search)
    .catch((error) => {
      console.error(error);
      return [];
    });
}

/**
 * Get movie from id.
 * @param {string} id
 * @returns {Movie[]}
 */
export async function getMovie(id: string): Promise<MovieInfo | undefined> {
  return fetch(
    `${BASE_URL}?apikey=${API_KEY}&i=${id}`,
    {
      method: "GET",
    }
  )
    .then((r) => r.json())
    .catch((error) => {
      console.error(error);
      return {};
    });
}

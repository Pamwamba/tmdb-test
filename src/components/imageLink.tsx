import { Link } from "react-router-dom";
import styled from "styled-components";

import { Movie } from "../types";

const Image = styled.img`
  border-radius: 5px;
`;

/**
 * Prop types.
 */
type Props = {
  to: string,
  movie: Movie,
  src: string
}

const ImageLink = ({to, movie, src}: Props) => {
  return (
    <Link to={to}>
      <Image alt={movie.Title} key={movie.imdbID} src={src} width='200px' />
    </Link>
  )
}

export default ImageLink;
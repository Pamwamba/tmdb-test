import { Link, useLocation } from 'react-router-dom';
import useLocalStorage from 'use-local-storage';
import Switch from "react-switch";
import { useState } from 'react';
import styled from 'styled-components';

import { ICONS } from '../config/AppConstant';

import './css/navBar.css';

/**
 * Prop types.
 */
type Props = {
  switchTheme: Function // Callback function to switch theme.
}

const Header = styled.header`
  background-color: var(--header);
  color: white;
  font-family: 'Roboto';
  justify-content: center;
  font-size: calc(10px + 2vmin);
  min-height: 0vh;
  text-align: center;
`;

const NavbarContent = styled.div`
  display: inline-flex;
  align-items: center;
`;

const SwitchContainer = styled.div`
  position: absolute;
  float: right;
  right: 0;
  margin-right: 5vh;
`

const Title = styled.h3`
  text-align: center;
`

const NavBar = ({switchTheme}: Props) => {
  const location = useLocation().pathname;
  const [isDark] = useLocalStorage('theme', true);
  const [isToggled, setIsToggled] = useState(isDark);

  const onToggle = () => {
    switchTheme();
    setIsToggled(!isToggled);
  };

  return (
    <Header>
      <link rel="preconnect" href="https://fonts.gstatic.com" />
      <link
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500&display=swap"
      rel="stylesheet"
      />
      <link
      href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet"
      />
      <NavbarContent>
        {/* Add back button if we're not in home page */}
        {location !== '/' && (
          <Link className='backLink' to='/'>
            <span className="material-icons">{ICONS.ARROW_BACK}</span>
          </Link>)}
        <Title>Movies</Title>
        <SwitchContainer>
          <span className="material-icons">{ICONS.LIGHT_MODE}</span>
          <Switch
            onChange={onToggle}
            checked={isToggled}
            checkedIcon={false}
            uncheckedIcon={false}
            onColor='#d2d2d2'
            offColor='#d2d2d2'
            onHandleColor='#fff'
            offHandleColor='#fff'
            height={14}
            handleDiameter={20}
            width={40}
            />
          <span className="material-icons">{ICONS.DARK_MODE}</span>
        </SwitchContainer>
      </NavbarContent>
    </Header>
  );
}

export default NavBar;
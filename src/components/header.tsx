import { useLocation } from "react-router-dom";
import { Movie } from "../types";
import NavBar from "./navBar";
import SearchBar from "./searchBar";

type Props = {
  getResults: Function,
  switchTheme: Function
}

const Header = ({switchTheme, getResults}: Props) => {
  const location = useLocation().pathname;

  return (
  <>
    <NavBar switchTheme={() => switchTheme()} />
    {location === '/' && (<SearchBar getResults={(m: Movie[]) => getResults(m)} />)}
  </>
)}

export default Header;
import { useParams } from "react-router-dom";
import styled from "styled-components";

import { getMovie } from "../api/moviesApi";
import { MESSAGES } from "../config/AppConstant";
import { useQuery } from "react-query";

const MovieContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin: auto;
  margin-top: 5vh;
  width: 70%;
  text-align: justify;

  @media (min-width: 1025px){
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    margin: auto;
    margin-top: 5vh;
    width: 70%;
    text-align: justify;
  }

  @media (min-width: 768px) and (max-width: 1024px) {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    margin: auto;
    margin-top: 5vh;
    width: 90%;
    text-align: justify;
  }

  @media (min-width: 320px) and (max-width: 767px) {
    display: flex;
    align-items: center;
    flex-direction: column;
    width: 100%;
    margin: auto;
    margin-top: 5vh;
    text-align: justify;
  }`;

const MovieOverview = styled.div`
  width: 70%;`;
const MovieImg = styled.img``;

const MovieDetails = () => {
  const { id } = useParams() as { id: string };


  const fetchData = async () => {
    const result = await getMovie(id);
    return result
  };

  const { data, status } = useQuery([`movie`, id], fetchData);

  if (status === "loading") {
    return ( <h1>{MESSAGES.LOADING}</h1>)
  }

  return data ? (
    <MovieContainer>
      <MovieOverview>
        <h1>{data.Title}</h1>
        <p>{data.Plot}</p>
        <i>{MESSAGES.CASTING} {data.Actors}</i>
        <p>{MESSAGES.RATING} {data.imdbRating} {MESSAGES.MAX_RATE}</p>
      </MovieOverview>
      <MovieImg key={data.imdbID} src={data.Poster} width='200px' />
    </MovieContainer>) :
    <div>
      <h1>{MESSAGES.NOT_FOUND}</h1>
    </div>
}
  export default MovieDetails;

import { useEffect, useState } from "react";
import styled from "styled-components";

import { Movie } from "../types";
import { MESSAGES } from '../config/AppConstant';
import ImageLink from "../components/imageLink";

/**
 * Prop types.
 */
type Props = {
  moviesList: Movie[] // The list of movies got by the search.
}

const MovieContainer = styled.div`
align-items: center;

  @media (min-width: 1025px) {
    width: 50%;
    margin:auto;
    column-gap: 1vh;
    row-gap: 1vh;
    display: grid;
    grid-template-columns: 0.5fr 0.5fr 0.5fr 0.5fr 0.5fr;
    justify-content: center;
    margin-top: 1vh;
  }

  @media (min-width: 768px) and (max-width: 1024px) {
    column-gap: 1vh;
    row-gap: 1vh;
    display: grid;
    grid-template-columns: 0.5fr 0.5fr 0.5fr 0.5fr;
    justify-content: center;
    margin-top: 1vh;
    background-color: var(--background);

  }

  @media (min-width: 320px) and (max-width: 767px) {
    display: grid;
    grid-template-columns: 0.5fr 0.5fr;
    justify-content: center;
    margin-top: 1vh;
    background-color: var(--background);
  }
`;

const MovieList = ({moviesList}: Props) => {
  const [moviesToDisplay, setMoviesToDisplay] = useState<Movie[]>([]); // The list of movies to display.

  // Update the list if something is written in the search bar.
  useEffect( () => {
    if (moviesList && moviesList.length > 0) {
      setMoviesToDisplay(moviesList)
    }
    if (!moviesList || (moviesList && moviesList.length === 0)) {
      setMoviesToDisplay([])
    }
  }, [moviesList])

  // Message if no result found.
  if (moviesToDisplay.length === 0) {
    return (<h1>{MESSAGES.NO_RESULT}</h1>)
  }

  return (
    <MovieContainer>
      {moviesToDisplay && moviesToDisplay.map(item => (
        <ImageLink key={item.imdbID} to={`/${item.imdbID}`} movie={item} src={item.Poster} />
      ))}
    </MovieContainer>
  );

}
  export default MovieList;
